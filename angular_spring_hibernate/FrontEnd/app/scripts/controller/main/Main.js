'use strict';

/**
 * @ngdoc function
 * @name angularJSHW.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularJSHW
 */
angular.module('angularJSHW')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
