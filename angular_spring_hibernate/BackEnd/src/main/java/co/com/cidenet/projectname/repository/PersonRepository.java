package co.com.cidenet.projectname.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.cidenet.projectname.model.Person;

/**
 * Repositorio de personas
 * permite hacer el CRUD
 * 
 * Recive el Modelo que va ha manejar y
 * el tipo de dato del ID de ese modelo
 * 
 * @author Cidenet
 *
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Integer>{

}

