package com.cidenet.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.cidenet.controller.JSONController;
import com.cidenet.model.Shop;
import com.cidenet.services.metodos;

public class JSONControllerTest {
	JSONController jc;
	metodos mt;

	@BeforeClass
	public static void beforeClassTest() {
		System.out.println("BeforeClass");
	}

	@Before
	public void beforeTest() {
		System.out.println("Before");
		jc = new JSONController();
		mt = new metodos();
		mt.deleteAll();
		mt.insertShop(1, "name_1");
		mt.insertShop(2, "name_2");
	}

	@Test
	public void miTest() {
		System.out.println("@Test - test_method_2");

		String prueba = "hola";
		assertEquals(prueba, "hola");

	}

	@Test
	public void isShopExistTest() {
		Shop shop = new Shop();
		Shop shopTest = new Shop();
		shop.setId(1);
		boolean s = mt.isShopExist(shop);
		assertEquals(s, true);
	}

	@Test
	public void sumaTest() {
		int esperado = 3;
		int resultado = mt.suma(1, 2);
		assertEquals(resultado, esperado);
	}

	@Test
	public void insertShopTest() {
		boolean resultado = mt.insertShop(4, "juan");
		assertEquals(resultado, true);
	}

	@Test
	public void updateShopTest() {
		int id = 1;
		String name = "santiagoA";
		mt.deleteAll();
		mt.insertShop(id, "pedro");

		Shop shopRespuesta = new Shop();

		shopRespuesta = mt.getShopById(id);
		mt.updateShop(id, name);
		shopRespuesta = mt.getShopById(id);
		mt.updateShop(id, name);
		System.out.println(shopRespuesta.getName());

		assertEquals(shopRespuesta.getName(), name);
	}

	@Test
	public void getShopByidTest() {
		int id = 2;

		Shop shopEsperado = new Shop();
		shopEsperado.setId(id);
		shopEsperado.setName("name_2");

		Shop shopRespuesta = mt.getShopById(id);
		System.out.println(shopRespuesta.getName());
		System.out.println(shopEsperado.getName());
		assertEquals(shopRespuesta.getName(), shopEsperado.getName());
	}

	@Test
	public void deleteShopTest() {
		System.out.println("delete");
		int id = 11;
        mt.deleteAll();
		mt.insertShop(id, "prueba");
		mt.deleteShop(id);

		Shop shopRespueta = mt.getShopById(id);
		assertNull(shopRespueta);
	}

	@Test
	public void deleteAllTest() {
		mt.deleteAll();
		int tam;
		
		List listRespuesta = mt.listShops();
		tam = listRespuesta.size();
		assertEquals(tam, 0);
	}
	
	@Test
	public void listAllTest() {
		mt.deleteAll();
		List<Shop> listShops = new ArrayList<Shop>();
		
		Shop shop1 =new Shop();
		shop1.setId(1);
		shop1.setName("name_1");
		
		Shop shop2 = new Shop();
		shop2.setId(2);
		shop2.setName("name_2");
		
		listShops.add(shop1);
		listShops.add(shop2);
		
		mt.insertShop(1, "name_1");
		mt.insertShop(2, "name_2");
		
		List<Shop> listRespuesta = mt.listShops();
		//assertSame(listRespuesta, listShops);
		assertTrue(listRespuesta.equals(listShops));
		
		System.out.println(listRespuesta.equals(listShops));
	}
	

	@AfterClass
	public static void afterClass() {
		System.out.println("Terminamos");
		
	}
}
