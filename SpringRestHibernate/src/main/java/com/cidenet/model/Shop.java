package com.cidenet.model;

import org.hibernate.annotations.Entity;

@Entity
public class Shop {

	int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Shop() {
	}

	@Override
	public boolean equals(Object o) {
		boolean match = true;

		if (!(o instanceof Shop)) {
			return false;
		}

		Shop s = (Shop) o;
		if (s.id != this.id) {
			match = false;
		}

		if (!s.getName().equalsIgnoreCase(this.name) ) {
			match = false;
		}
		return match;
	}

}