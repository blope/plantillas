package co.com.cidenet.projectname.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.com.cidenet.commons.dto.PersonDTO;
import co.com.cidenet.projectname.service.PersonService;

/**
 * Controlador que expone
 * el servicio REST de personas
 * 
 * @author Cidenet
 *
 */
@Controller
@RequestMapping("/people")
public class PersonContoller {
	
	@Autowired
	private PersonService personService;
	
	@RequestMapping(value="/all", method = RequestMethod.GET)
	public ResponseEntity<List<PersonDTO>> getPerson(){
		List<PersonDTO> people = personService.getAll();
		return new ResponseEntity<List<PersonDTO>>(people, HttpStatus.OK);
	}
}
