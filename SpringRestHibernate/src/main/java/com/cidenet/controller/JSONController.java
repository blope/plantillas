package com.cidenet.controller;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import com.cidenet.model.Shop;
import com.cidenet.services.metodos;
import com.cidenet.util.HibernateUtil;

@Controller
@RequestMapping("/metodo")
public class JSONController {
	metodos metodo = new metodos();

	@RequestMapping(value = "{ActualizarName}", method = RequestMethod.GET)
	public @ResponseBody Shop getShopInJSON(@PathVariable String name) {

		Shop shop = new Shop();
		shop.setName(name);
		return shop;

	}

	@RequestMapping(value = "/test/{name}", method = RequestMethod.GET)
	public ResponseEntity<String> test(@PathVariable String name) {

		return new ResponseEntity<String>("hola " + name, HttpStatus.OK);

	}

	@RequestMapping(value = "/suma/numero1/{Number1}/numero2/{Number2}", method = RequestMethod.GET)
	public ResponseEntity<String> sumaRest(@PathVariable int Number1, @PathVariable int Number2) {
		int suma = metodo.suma(Number1, Number2);
		return new ResponseEntity<String>(Integer.toString(suma), HttpStatus.OK);

	}

	@RequestMapping(value = "shop/insert/id/{id}/name/{name}", method = RequestMethod.PUT)
	public ResponseEntity<Void> insertarShopRest(@PathVariable int id, @PathVariable String name) {
		boolean fallo = metodo.insertShop(id, name);
		if (fallo == true)
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		else {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

	}

	@RequestMapping(value = "shop/get/id/{id}", method = RequestMethod.GET)
	public ResponseEntity<Shop> getShopByIdRest(@PathVariable int id) {
		Shop shop = metodo.getShopById(id);
		return new ResponseEntity<Shop>(shop, HttpStatus.OK);
	}

	@RequestMapping(value = "shop/update/id/{id}/name/{name}", method = RequestMethod.PUT)
	public ResponseEntity<Void> updateShopRest(@PathVariable int id, @PathVariable String name) {
		metodo.updateShop(id, name);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "shop/delete/id/{id}", method = RequestMethod.PUT)
	public void deleteShopRest(@PathVariable int id) {
		metodo.deleteShop(id);
	}

	@RequestMapping(value = "shop/todos", method = RequestMethod.GET)
	public ResponseEntity<List> listShopsRest() {
		List<Shop> shops = metodo.listShops();
		return new ResponseEntity<List>(shops, HttpStatus.OK);
	}

}