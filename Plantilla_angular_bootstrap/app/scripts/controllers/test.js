'use strict';

/**
 * @ngdoc function
 * @name treeAndPieApp.controller:TestCtrl
 * @description
 * # TestCtrl
 * Controller of the treeAndPieApp
 */
angular.module('treeAndPieApp')
.controller('TestCtrl', function ($scope) {

	/* backgroundColor: [
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 159, 64, 0.2)'
				],
				borderColor: [
					'rgba(54, 162, 235, 1)',
					'rgba(255, 159, 64, 1)'
				], */

	$scope.data = [
		{ "valor" : "Antioquia", "etiqueta" : "Departamento", "id" : "role1", "canLeido" : "2", "canNoLeido": "8", "listaAgrupamientos" : [
			{ "valor" : "Envigado", "etiqueta" : "Ciudad", "id" : "role11", "canLeido" : "5", "canNoLeido": "1", "listaAgrupamientos" : [] },
			{ "valor" : "Medellín", "etiqueta" : "Ciudad", "id" : "role12", "canLeido" : "10", "canNoLeido": "0", "listaAgrupamientos" : [
				{ "valor" : "Belén", "etiqueta" : "Barrio", "id" : "role121", "canLeido" : "0", "canNoLeido": "8", "listaAgrupamientos" : [
					{ "valor" : "Corazón", "etiqueta" : "Sub-Barrio", "id" : "role1211", "canLeido" : "7", "canNoLeido": "12", "listaAgrupamientos" : [] },
					{ "valor" : "La palma", "etiqueta" : "Sub-Barrio", "id" : "role1212", "canLeido" : "15", "canNoLeido": "8", "listaAgrupamientos" : [] }
				]}
			]}
		]},
		{ "valor" : "Cundinamarca", "etiqueta" : "Departamento", "id" : "role2", "canLeido" : "5", "canNoLeido": "5", "listaAgrupamientos" : [] },
		{ "valor" : "Chocó", "etiqueta" : "Departamento", "id" : "role3", "canLeido" : "3", "canNoLeido": "2", "listaAgrupamientos" : [] }
	];
	
	$scope.chart = {
		model: {
			labels: ["Leidos", "No Leidos"],
			datasets: [{
				data: [0, 0],
				backgroundColor: [
					'rgba(48, 90, 251, 0.58)',
					'rgba(149, 157, 162, 0.3)'
				],
				borderColor: [
					'rgba(48, 90, 251, 1)',
					'rgba(149, 157, 162, 1)'
				],
				borderWidth: 0.5
			}]
		},
		options: {
			tooltips: {
				callbacks: {
					label: function(tooltipItem, data) {
						var allData = data.datasets[tooltipItem.datasetIndex].data;
						var tooltipLabel = data.labels[tooltipItem.index];
						var tooltipData = allData[tooltipItem.index];
						var total = 0;
						for (var i in allData) {
							total += +allData[i];
						}
						var tooltipPercentage = Math.round((tooltipData / total) * 100);
						return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
					}
				}
			}
		}
	}
	
	$scope.$watch( 'list.currentNode', function( newObj, oldObj ) {
		if( $scope.list && angular.isObject($scope.list.currentNode) ) {
			var curr = $scope.list.currentNode;
			$scope.chart.model.datasets[0].data = [curr.canLeido, curr.canNoLeido];
		}
	}, false);
	
	function getLabels(node){
		if(node.listaAgrupamientos && node.listaAgrupamientos.length > 0){
			angular.forEach(node.listaAgrupamientos, function(agrupamiento){
				getLabels(agrupamiento);
			});
		}
		node.label = node.etiqueta + " : " + node.valor;
	}
	
	function init(){
		angular.forEach($scope.data, function(node){
			getLabels(node);
		});
		console.log($scope.data);
	}
	
	init();
});
