package co.com.cidenet.commons.dto;

import java.util.List;

public class PersonDTO extends DTO {
	
	private String name;
    
    private String country;
    
    private List<TaskDTO> tasksDTO;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<TaskDTO> getTasksDTO() {
		return tasksDTO;
	}

	public void setTasksDTO(List<TaskDTO> tasks) {
		this.tasksDTO = tasks;
	}
}
