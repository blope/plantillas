'use strict';

/**
 * @ngdoc overview
 * @name angularJSHW
 * @description
 * # angularJSHW
 *
 * Main module of the application.
 */
angular
  .module('angularJSHW', [
	'ngMaterial',
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'scripts/controller/main/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
	  .when('/people', {
        templateUrl: 'scripts/controller/people/people.html',
        controller: 'PeopleCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
