'use strict';

/**
 * @ngdoc function
 * @name treeAndPieApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the treeAndPieApp
 */
angular.module('treeAndPieApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma 2'
    ];
  });
