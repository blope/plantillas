package co.com.cidenet.projectname.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import co.com.cidenet.commons.dto.PersonDTO;
import co.com.cidenet.projectname.utils.DTOList;
 
/**
 * Modelo de persona
 * 
 * @author Cidenet
 *
 */
@Entity
@Table(name="PERSON")
public class Person extends Model<PersonDTO>{
	
	//ID autogenerado con la secuencia: S_PERSON_ID
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_PERSON_ID")
	@SequenceGenerator(name = "S_PERSON_ID", sequenceName = "S_PERSON_ID", allocationSize = 1, initialValue = 1)
    private int id;
     
    private String name;
     
    private String country;
    
    //Lista de tareas de una persona
    @OneToMany(fetch = FetchType.LAZY, mappedBy="person")
    private List<Task> tasks;
 
    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getCountry() {
        return country;
    }
 
    public void setCountry(String country) {
        this.country = country;
    }

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	@Override
	public Class<PersonDTO> getDtoClass() {
		return PersonDTO.class;
	}    
	
	@Override
	public PersonDTO getDto() {
		PersonDTO p = super.getDto();
		List<Task> tasks = this.getTasks();
		p.setTasksDTO(DTOList.getDtoList(tasks));
		return p;
	}
}