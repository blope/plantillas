'use strict';

/**
 * @ngdoc function
 * @name angularJSHW.controller:PeopleCtrl
 * @description
 * # PeopleCtrl
 * Controller of the angularJSHW
 */
angular.module('angularJSHW')
.controller('PeopleCtrl', function ($scope, PeopleFactory) {
	
	function init(){
		PeopleFactory.getPeople().then(
			function success(response){
				$scope.people = response;
			},
			function error(reason){
				alert("Error cargando personas");
			}
		);
	}
	
	init();
});
