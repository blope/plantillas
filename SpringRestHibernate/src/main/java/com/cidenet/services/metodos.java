package com.cidenet.services;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.cidenet.model.Shop;
import com.cidenet.util.HibernateUtil;

public class metodos {
	public int suma(int Number1, int Number2) {
		int suma = Number1 + Number2;
		return suma;
	}

	public boolean insertShop(int id, String name) {
		boolean fallo = true;
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
		Shop shop = new Shop();

		shop.setName(name);
		shop.setId(id);

		if (isShopExist(shop)) {
			fallo = false;
		}

		session.save(shop);
		session.getTransaction().commit();

		return fallo;

	}

	public boolean isShopExist(Shop shop) {
		boolean s = false;
		Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		Shop shopBD = new Shop();
		shopBD = (Shop) session.get(Shop.class, shop.getId());

		Hibernate.initialize(shopBD);

		if (shopBD != null) {
			s = true;
		}

		return s;

	}

	public Shop getShopById(int id) {
		Session session = null;
		Shop shop = new Shop();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			shop = (Shop) session.get(Shop.class, id);
			Hibernate.initialize(shop);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return shop;
	}

	public void updateShop(int id, String name) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Shop shop = new Shop();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			shop = (Shop) session.get(Shop.class, id);
			shop.setName(name);
			session.update(shop);
			tx.commit();
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	public void deleteShop(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Shop shop = new Shop();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			shop = (Shop) session.get(Shop.class, id);
			session.delete(shop);
			tx.commit();
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	public List<Shop> listShops() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Shop> shops = new ArrayList<Shop>();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			shops = session.createQuery("FROM Shop").list();
			if (!tx.wasCommitted()){
			    tx.commit();
			}
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return shops;

	}

	public void deleteAll() {
		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("delete Shop");
			query.executeUpdate();
			if (!tx.wasCommitted()){
			    tx.commit();
			}
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

	}

}
