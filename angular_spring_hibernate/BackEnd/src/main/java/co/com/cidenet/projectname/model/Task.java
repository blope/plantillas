package co.com.cidenet.projectname.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import co.com.cidenet.commons.dto.TaskDTO;

/**
 * Modelo de tarea
 * 
 * @author Cidenet
 *
 */
@Entity
@Table(name="TASK")
public class Task extends Model<TaskDTO>{
	
	//ID autogenerado con la secuencia: S_TASK_ID
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_TASK_ID")
	@SequenceGenerator(name = "S_TASK_ID", sequenceName = "S_TASK_ID", allocationSize = 1, initialValue = 1)
    private int id;
    
	@Column
    private String name;
    
	//Relación con persona
	//Cada tarea tiene una persona asociada
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="PERSON")
    private Person person;
    

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public Class<TaskDTO> getDtoClass() {
		return TaskDTO.class;
	}

}
