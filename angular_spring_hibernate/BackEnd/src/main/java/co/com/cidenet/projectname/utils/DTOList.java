package co.com.cidenet.projectname.utils;

import java.util.ArrayList;
import java.util.List;

import co.com.cidenet.commons.dto.DTO;
import co.com.cidenet.projectname.model.Model;

@SuppressWarnings("rawtypes")
public class DTOList {
	
	
	/**
	 * Convierte una lista de Model a una lista de DTO
	 * 
	 * @param dtos Lista destino
	 * @param models Lista origen
	 */
	@SuppressWarnings("unchecked")
	public static <T extends DTO> List<T> getDtoList(List<? extends Model<T>> models){
		
		List<T> dtos = new ArrayList<>();
		
		if(models != null){
			for (Model model : models) {				
				T dto = (T) model.getDto();
				dtos.add(dto);				
			}
		}
		
		return dtos;
	}
}
