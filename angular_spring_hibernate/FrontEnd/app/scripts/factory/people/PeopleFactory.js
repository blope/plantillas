'use strict';

/**
 * @ngdoc service
 * @name angularJSHW.PeopleFactory
 * @description
 * 
 * Factory of the angularJSHW
 */
angular.module('angularJSHW')
  .factory('PeopleFactory', function (NavigationConstant, $resource) {
	  
	/** Configuración de la URL base del servicio */
	var urlBase = NavigationConstant.SERVER.HOST + NavigationConstant.SERVER.SERVICE;
	var processUrl = 'people/';
	
	function getAll(){
		var service = $resource(urlBase + processUrl + 'all');
        return service.query().$promise;
	}
  
	return {
		getPeople : getAll
	};
});