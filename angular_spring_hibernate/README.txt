Front End:
- AngularJS
- Twitter Bootstrap
- Bower
- Npm

Back End:
- Spring
	- Rest
	- MVC
	- JPA
- Hibernate
- Oracle