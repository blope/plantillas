'use strict';

/**
 * @ngdoc service
 * @name angularJSHW.NavigationConstant
 * @description
 * # NavigationConstant
 * Constants in the angularJSHW.
 */
angular.module('angularJSHW')
  .constant('NavigationConstant', {
	
	SERVER: {
		HOST:'http://localhost:8080/',
		SERVICE: 'ProjectName/services/'
	}
});