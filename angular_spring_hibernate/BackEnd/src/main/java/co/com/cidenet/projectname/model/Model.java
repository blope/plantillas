package co.com.cidenet.projectname.model;

import org.springframework.beans.BeanUtils;

import co.com.cidenet.commons.dto.DTO;

/**
 * Clase generiaca que permite
 * definir los modelos desde BD
 * 
 * @author Cidenet
 *
 * @param <T> Clase del equivalente DTO
 */
public abstract class Model <T extends DTO> {
	
	/**
	 * Obtiene la versión DTO del model, 
	 * si la implementación requiere convertir datos complejos se debe
	 * reescribir
	 * 
	 * @return
	 */
	public T getDto() {
		try {
			T dto = this.getDtoClass().newInstance();
			BeanUtils.copyProperties(this, dto);
			return dto;
		} catch (InstantiationException | IllegalAccessException | NullPointerException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public abstract Class<T> getDtoClass();
}
