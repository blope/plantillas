package co.com.cidenet.projectname.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.cidenet.commons.dto.PersonDTO;
import co.com.cidenet.projectname.model.Person;
import co.com.cidenet.projectname.repository.PersonRepository;
import co.com.cidenet.projectname.utils.DTOList;


/**
 * Servicio encargado de
 * manejar la logica de negocio
 * que consierne a personas
 * 
 * @author Cidenet
 *
 */
@Service("personService")
public class PersonService {
	
	@Autowired
	private PersonRepository personRepository;
	
	/**
	 * Obtiene la lista
	 * de todas las personas
	 * 
	 * @return {@code List<PersonDTO>} lista de personas
	 */
	@Transactional(readOnly=true)
	public List<PersonDTO> getAll(){
		List<PersonDTO> people = new ArrayList<PersonDTO>();
		List<Person> peps = (List<Person>) personRepository.findAll();
		if(peps != null){
			people = DTOList.getDtoList(peps);
		}
		return people;
	}
}
