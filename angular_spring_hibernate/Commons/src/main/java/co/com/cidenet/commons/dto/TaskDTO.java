package co.com.cidenet.commons.dto;

public class TaskDTO extends DTO {
	
	private String name;
	private PersonDTO person;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public PersonDTO getPerson() {
		return person;
	}
	public void setPerson(PersonDTO person) {
		this.person = person;
	}
}
